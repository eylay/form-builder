<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('user/current', 'UserController@current');

// manageing forms
Route::get('form/index', 'FormController@index');
Route::get('form/result/{id}', 'FormController@result')->where('id', '[0-9]+');
Route::get('form/{id}', 'FormController@show')->where('id', '[0-9]+');
Route::delete('form/{form}', 'FormController@destroy')->where('form', '[0-9]+');
Route::post('form/upsert', 'FormController@upsert');
Route::delete('form/answers/{applicant}', 'FormController@deleteAnswers')->where('applicant', '[0-9]+');

// form filling
Route::get('form/list', 'FormFillingController@list');
Route::get('form/uid/{uid}', 'FormFillingController@getForm');
Route::get('form/answers/{form_id}/{user_id?}', 'FormFillingController@getAnswers');
Route::post('form/uid/{uid}', 'FormFillingController@fillForm');

// file upload
Route::post('form/fill/file/upload', 'FileController@formFillUpload');

// city and states
Route::get('all-states-and-cities', 'CitiesAndStatesController@allStatesAndCities');
Route::get('states', 'CitiesAndStatesController@states');
Route::get('cities/{state}', 'CitiesAndStatesController@cities');
Route::get('cities/selected/{formUid}', 'CitiesAndStatesController@selectedStates');

// extract from db
Route::get('extract/{table}/{col}', 'FormFillingController@extract');
