<!-- sidebar left start-->
<div class="sidebar-right">
    <div class="sidebar-right-info">

        <div class="user-box">
            <div class="d-flex justify-content-center">
                <img src="{{asset('assets/images/users/avatar-1.jpg')}}" alt="" class="img-fluid rounded-circle">
            </div>
            <div class="text-center text-white mt-2">
                <h6>{{user('name')}}</h6>
                <p class="text-muted m-0">{{user('type')}}</p>
            </div>
        </div>

        <!--sidebar nav start-->
        <ul class="side-navigation">
            <li>
                <h3 class="navigation-title"> منوی اصلی </h3>
            </li>
            <li class="active">
                <a href="#/home"><i class="mdi mdi-gauge"></i> <span> داشبورد اصلی </span> </a>
            </li>
            @if (user()->isAdmin())
                <li class="menu-list">
                    <a href="javascript:void(0)">
                        <i class="mdi mdi-format-float-left"></i>
                        <span> ماژول فرم ساز </span>
                    </a>
                    <ul class="child-list">
                        <li><a href="#/form-manager"> <i class="mdi mdi-plus"></i> طراحی فرم جدید </a></li>
                        <li><a href="#/form-list"> <i class="mdi mdi-format-list-bulleted"></i> لیست فرم ها </a></li>
                    </ul>
                </li>
            @endif

        </ul><!--sidebar nav end-->
    </div>
</div><!-- sidebar left end-->
