<?php

function user($p = null)
{
    $user = auth()->user();
    return $user ? ($p ? $user->$p : $user) : null;
}

function persianDateStringToTimestamp($persianDateString, $format = 'Y/m/d') {
    return \Morilog\Jalali\Jalalian::fromFormat($format, $persianDateString)->getTimestamp();
}
