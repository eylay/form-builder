<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use App\Models\Applicant;
use App\Models\Answer;
use App\Models\User;
use App\Models\FormItem;
use Illuminate\Validation\Rule;
use DB;
use Schema;

class FormFillingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list()
    {
        return Form::whereActive(1)->latest()->with('items')->get();
    }

    public function extract($table, $col)
    {
        return Schema::hasColumn($table, $col) ? DB::table($table)->pluck($col) : [];
    }

    public function getForm($uid)
    {
        return Form::whereActive(1)->where('uid', $uid)->with('items')->first();
    }

    public function getAnswers($form_id, $user_id=null)
    {
        if (!user()->isAdmin()) {
            $user_id = auth()->id();
        }
        $user = User::findOrFail($user_id);
        $applicant = $user->application($form_id);
        return ($applicant && $applicant->answers) ? $applicant->answers->pluck('body', 'form_item_id') : [];
    }

    public function fillForm($uid, Request $request)
    {
        // prepare data
        $form = Form::where('uid', $uid)->first();
        $rawAnswers = $request->all();

        // form validation
        $validationErrors = self::prepareValidationErrors($form, $rawAnswers);

        if (count($validationErrors)) {

            // display validation errors
            return ['success' => false, 'errors' => $validationErrors];

        }else {

            // update other database tables and coloumns
            if ($form->tbl && $form->key && $form->keyval && $form->fields && $form->fields->count()) {
                $dbData = [];
                foreach ($form->fields as $field) {
                    $value = $rawAnswers[$field->item];
                    $dbData[$field->name] = $value;
                }
                DB::table($form->tbl)->where($form->key, $form->keyval)->update($dbData);
            }

            // store answers in database
            $applicant = Applicant::firstOrCreate([
                'form_id' => $form->id,
                'user_id' => auth()->id()
            ]);
            foreach ($rawAnswers as $form_item_id => $body) {
                if ($form->getItem($form_item_id)) {
                    Answer::updateOrCreate(
                        ['applicant_id' => $applicant->id, 'form_item_id' => $form_item_id],
                        ['body' => $body]
                    );
                }
            }
            return ['success' => true];

        }

    }

    private static function prepareValidationErrors($form, $rawAnswers)
    {
        $validationErrors = [];
        foreach ($form->items as $item) {

            $userInput = $rawAnswers[$item->id] ?? null;

            // required or nullable
            if ($item->required && !$userInput) {
                $validationErrors []= "$item->title الزامی است.";
            }

            // required validation rule
            if ($userInput && $item->unique) {
                $applicant = Applicant::where('user_id', auth()->id())->where('form_id', $form->id)->first();
                $found = Answer::where('body', $userInput)->where('applicant_id', '!=', $applicant->id ?? 0)->where('form_item_id', $item->id)->first();
                if ($found) {
                    $validationErrors []= "$item->title قبلا استفاده شده است.";
                }
            }

            // validation base on item type : number
            if ($userInput && $item->type == 'number' && !is_numeric($userInput)) {
                $validationErrors []= "$item->title باید عدد باشد.";
            }

            // validation base on item type : select and radio (must be in array of choices)
            if ($userInput && ($item->type == 'select' || $item->type == 'radio')) {
                $list = explode("\n", $item->choices);
                if (!in_array($userInput, $list)) {
                    $validationErrors []= "$item->title انتخاب شده صحیح نیست.";
                }
            }

            // min and max validation
            if ($userInput) {
                // min validation
                if ($min = $item->min) {
                    if ($item->type == 'text' || $item->type == 'textarea') {
                        if (mb_strlen($userInput) < $min) {
                            $validationErrors []= "$item->title حداقل باید $min کاراکتر باشد.";
                        }
                    }elseif ($item->type == 'date') {
                        $minTimestamp = persianDateStringToTimestamp($min);
                        $userInputTimestamp = persianDateStringToTimestamp($userInput);
                        if ($userInputTimestamp < $minTimestamp) {
                            $validationErrors []= "$item->title باید بعد از تاریخ $min باشد.";
                        }
                    }elseif ($item->type == 'number') {
                        if ($userInput < $min) {
                            $validationErrors []= "$item->title باید از $min بزرگتر باشد.";
                        }
                    }
                }

                // max validation
                if ($max = $item->max) {
                    if ($item->type == 'text' || $item->type == 'textarea') {
                        if (mb_strlen($userInput) > $max) {
                            $validationErrors []= "$item->title حداکثر باید $max کاراکتر باشد.";
                        }
                    }elseif ($item->type == 'date') {
                        $maxTimestamp = persianDateStringToTimestamp($max);
                        $userInputTimestamp = persianDateStringToTimestamp($userInput);
                        if ($userInputTimestamp > $maxTimestamp) {
                            $validationErrors []= "$item->title باید قبل از تاریخ $max باشد.";
                        }
                    }elseif ($item->type == 'number') {
                        if ($userInput > $max) {
                            $validationErrors []= "$item->title باید از $min کوچکتر باشد.";
                        }
                    }
                }
            }

            // special cases validation : national code
            if ($userInput && $item->type == 'national_code') {
                if(strlen($userInput) != 10 ){
                    $ok = false;
                }else {
                    $sum = 0;
                    for ($i=-10; $i < 0 ; $i++) {
                        $number = substr($userInput, $i, 1);
                        if ( abs($i) > 1 ){
                            $sum += abs($i) * $number;
                        } else {
                            $control_number = $number;
                        }
                    }
                    $Q = floor($sum/11);
                    $R = $sum%11;

                    if( $R < 2 ){
                        $ok = $control_number == $R;
                    }else {
                        $ok = $control_number == (11-$R);
                    }
                }
                if (!$ok) {
                    $validationErrors []= "$item->title وارد شده صحیح نیست.";
                }
            }

        }
        return $validationErrors;
    }
}
