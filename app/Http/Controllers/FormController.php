<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use App\Models\FormItem;
use App\Models\FormField;
use App\Models\Applicant;
use App\Models\Answer;
use App\Models\City;
use Storage;

class FormController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        return Form::latest()->with('items')->get();
    }

    public function result($id)
    {
        $form = Form::findOrFail($id);
        $applicants = Applicant::where('form_id', $form->id)->with(['user'])->get();
        $answers = [];

        foreach ($applicants as $applicant) {

            // get raw answers in db
            $rawAnswersStoredInDB = Answer::where('applicant_id', $applicant->id)->pluck('body', 'form_item_id');

            // data modifications
            foreach ($rawAnswersStoredInDB as $form_item_id => $body) {
                $item = FormItem::findOrFail($form_item_id);

                // data modification for city and state
                if ($item->type == 'city') {
                    $city = City::find($body);
                    $rawAnswersStoredInDB[$form_item_id] = "{$city->state->name} - {$city->name}";
                }

            }

            // set final answers in variable to return
            $answers[$applicant->id] = $rawAnswersStoredInDB;
        }

        return compact('applicants', 'answers');
    }

    public function show($id)
    {
        return Form::where('id', $id)->with(['items', 'applicants', 'fields'])->first();
    }

    public function upsert(Request $request)
    {
        // define formId variable if present
        $formId = $request->form['id'] ?? 0;

        // validate incoming request
        $request->validate([
            'form.name' => 'required|string',
            'form.active' => 'nullable|boolean',
            'form.justify' => 'required|string',
            'form.tbl' => 'nullable|string',
            'form.key' => 'nullable|string',
            'form.keyval' => 'nullable|string',
            'items.*.width' => 'required|integer|min:1|max:12',
            'items.*.type' => 'required|string',
            'items.*.title' => 'required|string',
            'items.*.required' => 'nullable|boolean',
            'items.*.unique' => 'nullable|boolean',
            'items.*.min' => 'nullable',
            'items.*.max' => 'nullable',
            'items.*.choices' => 'nullable|string',
            'items.*.table' => 'nullable|string',
            'items.*.col' => 'nullable|string',
            'fields.*.name' => 'nullable|string',
            'fields.*.item' => 'nullable|integer',
        ]);

        // create or update form in db
        $formData = $request->form;
        if (!$formId) {
            $formData['uid'] = bin2hex(random_bytes(6)); // random string : 12 characters, only 0-9a-f
        }
        $form = Form::updateOrCreate(['id' => $formId], $formData);

        // create or update form items in db
        foreach ($request->items as $index => $itemData) {
            $itemData['form_id'] = $form->id;
            $itemData['position'] = $index+1;
            FormItem::updateOrCreate(['id' => $itemData['id'] ?? 0], $itemData);
        }

        // create or update form fields in db
        foreach ($request->fields as $index => $fieldsData) {
            if (isset($fieldsData['name']) && $fieldsData['name'] && isset($fieldsData['item']) && $fieldsData['item']) {
                $fieldsData['form_id'] = $form->id;
                FormField::where('form_id', $form->id)->delete();
                FormField::create($fieldsData);
            }
        }

        // delete those items which are in delete list
        if ($request->delete_list && count($request->delete_list)) {
            FormItem::whereIn('id', $request->delete_list)->delete();
        }

        return ['success' => true];
    }

    public function deleteAnswers(Applicant $applicant)
    {
        // delete uploaded file if this applicant has uploaded one
        $form = $applicant->form;
        foreach ($form->fileFields as $formItem) {
            $filePath = $formItem->getAnswerBody($applicant->id);
            Storage::disk('public')->delete(str_replace("/storage", "", $filePath));
        }

        // delete answers of this applicant
        Answer::where('applicant_id', $applicant->id)->delete();

        // delete applicant instance and finish
        $applicant->delete();
        return ['success' => true];
    }

    public function destroy(Form $form)
    {
        $form->delete(); // uses soft delete
        return ['success' => true];
    }
}
