<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use App\Models\City;
use App\Models\Form;
use App\Models\FormItem;
use App\Models\Applicant;
use App\Models\Answer;

class CitiesAndStatesController extends Controller
{
    public function states()
    {
        return State::all();
    }

    public function cities(State $state)
    {

        return $state->cities;
    }

    public function allStatesAndCities()
    {
        return [
            'states' => State::all(),
            'cities' => City::all(),
        ];
    }

    public function selectedStates($formUid)
    {
        $selectedStates = [];

        $form = Form::whereUid($formUid)->firstOrFail();
        $items = FormItem::Where('type', 'city')->where('form_id', $form->id)->pluck('id');

        $applicant = Applicant::where('user_id', auth()->id())->where('form_id', $form->id)->first();
        if ($applicant) {
            $answers = Answer::where('applicant_id', $applicant->id)->whereIn('form_item_id', $items)->pluck('body');
            foreach ($answers as $city_id) {
                if ($city = City::find($city_id)) {
                    $selectedStates []= $city->state_id;
                }
            }
        }

        return $selectedStates;
    }
}
