<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Models\FormItem;
use App\Models\Applicant;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function formFillUpload(Request $request)
    {
        // get item and item type from database
        $item = FormItem::whereIn('type', ['file', 'image'])->where('id', $request->item)->firstOrFail();
        $fieldType = $item->type;

        // perform file validation
        $maxValidationRule = $item->max ? "|max:$item->max" : "";
        $minValidationRule = $item->min ? "|min:$item->min" : "";
        $request->validate([
            'file' => 'required|'. $fieldType . $maxValidationRule . $minValidationRule
        ]);

        // delete previous file if any
        if ($request->oldfile != "undefined") {
            Storage::disk('public')->delete(str_replace("/storage", "", $request->oldfile));
        }

        // upload new file
        $file = $request->file('file');
        $dir = 'public/files';
        $path = $file->store($dir);

        // return path of the new uploaded file
        return "/storage/files/".str_replace("$dir/", "", $path);

    }
}
