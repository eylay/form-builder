<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Schema;

class FormItem extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $appends = ['options'];

    public function getOptionsAttribute()
    {
        if ($this->tbl && $this->col && Schema::hasColumn($this->tbl, $this->col)) {
            return DB::table($this->tbl)->pluck($this->col);
        }elseif ($this->choices) {
            return explode("\n", $this->choices);
        }else {
            return [];
        }
    }

    public function getAnswerBody($applicant_id)
    {
        $answer = Answer::where('form_item_id', $this->id)->where('applicant_id', $applicant_id)->first();
        return $answer ? $answer->body : null;
    }
}
