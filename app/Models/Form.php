<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = ['id'];

    public function items()
    {
        return $this->hasMany(FormItem::class)->orderBy('position');
    }

    public function fields()
    {
        return $this->hasMany(FormField::class);
    }

    public function fileFields()
    {
        return $this->hasMany(FormItem::class)->whereType('file');
    }

    public function applicants()
    {
        return $this->hasMany(Applicant::class)->latest();
    }

    public function getItem($item_id)
    {
        return FormItem::where('form_id', $this->id)->where('id', $item_id)->first();
    }

}
