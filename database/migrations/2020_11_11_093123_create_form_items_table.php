<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('form_id');
            $table->unsignedSmallInteger('position');
            $table->string('type');
            $table->string('title')->nullable();
            $table->unsignedSmallInteger('width');
            $table->string('min')->nullable();
            $table->string('max')->nullable();
            $table->boolean('required')->default(0);
            $table->boolean('unique')->default(0);
            $table->text('choices')->nullable();
            $table->string('tbl')->nullable();
            $table->string('col')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_items');
    }
}
