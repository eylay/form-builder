<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            AdminSeeder::class,
            UserSeeder::class,
        ]);

        $cities = 'database/sql/cities.sql';
        $states = 'database/sql/states.sql';

        DB::unprepared(file_get_contents($cities));
        DB::unprepared(file_get_contents($states));

    }
}
