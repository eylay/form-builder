<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'user1',
                'email' => 'user1',
                'email_verified_at' => now(),
                'password' => bcrypt('user1'),
                'type' => 'USER',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'user2',
                'email' => 'user2',
                'email_verified_at' => now(),
                'password' => bcrypt('user2'),
                'type' => 'USER',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
